import math
import cmath

# Функция для проверки корректности ввода числа
def get_number(prompt):
    while True:
        try:
            number = float(input(prompt))
            return number
        except ValueError:
            print("Ошибка: введите корректное число.")

# Запрос операции и чисел от пользователя
operation = input("Выберите операцию (+, -, *, /, sin, cos, tan, sqrt, log, exp, real, imag): ")
if operation in ["sin", "cos", "tan", "sqrt", "log", "exp"]:
    number = complex(get_number("Введите действительную часть комплексного числа: "), get_number("Введите мнимую часть комплексного числа: "))
else:
    number1 = complex(get_number("Введите действительную часть первого комплексного числа: "), get_number("Введите мнимую часть первого комплексного числа: "))
    number2 = complex(get_number("Введите действительную часть второго комплексного числа: "), get_number("Введите мнимую часть второго комплексного числа: "))

# Выполнение операции и вывод результата
if operation == "+":
    print(number1 + number2)
elif operation == "-":
    print(number1 - number2)
elif operation == "*":
    print(number1 * number2)
elif operation == "/":
    if number2 == 0:
        print("Ошибка: деление на ноль.")
    else:
        print(number1 / number2)
elif operation == "sin":
    print(cmath.sin(number))
elif operation == "cos":
    print(cmath.cos(number))
elif operation == "tan":
    print(cmath.tan(number))
elif operation == "sqrt":
    print(cmath.sqrt(number))
elif operation == "log":
    print(cmath.log(number))
elif operation == "exp":
    print(cmath.exp(number))
elif operation == "real":
    print(number1.real)
    print(number2.real)
elif operation == "imag":
    print(number1.imag)
    print(number2.imag)
else:
    print("Вы выбрали недопустимую операцию.")
