FROM ubuntu:latest

# Copy the current directory contents into the container at /app
COPY . /app

# Set the working directory to /app
WORKDIR /app

RUN apt-get update -y && apt-get install -y python3-pip 

